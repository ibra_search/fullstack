# fullstack (Non finalisé !!)

<h2> Définition </h2>
Ce projet est basé sur deux langages programmations : 
<ul>
    <li>
        Java Spring boot (demo-book-back)
    </li>
    <li>
        Angular (book-front)
    </li>
</ul>

<p>Ce mini projet est avant tout de montrer mes compétences, mes connaissances sur les technologies.</p>
<p>Le projet s'appelle Book (livre en anglais), je montre les minimums sur l'ensemble technologies</p>
<p>J'ai crée deux microservices : </p>
<ul>
    <li>La partie back-end contient principalement le book, cette partie permet de traiter, créer, supprimer ou modifier les books et les catégories, authentification avec JWT.</li>
    <li>La partie front-end contient principalement la partie d'écran, c'est pour afficher les books et connexion utilisateur.</li>
</ul> 
<p>Pour bien fonctionner les deux micro services, j'ai ajouté la technologie Docker qui permet conterniser ces deux microservices.</p>
<p>L'avantage micro service est nombreux : </p>
<ul>
    <li>Complètement indépendant</li>
    <li>Petit service</li>
    <Li>Facilitation du travail aux développeurs</Li>
    <li>Communication entre différent services</li>
</ul>
<p>
In short, the microservice architectural style [1] is an approach to developing a single application as a suite of small services, each running in its own process and communicating with lightweight mechanisms, often an HTTP resource API. These services are built around business capabilities and independently deployable by fully automated deployment machinery. There is a bare minimum of centralized management of these services, which may be written in different programming languages and use different data storage technologies.
</p>
<p>Source : <a href="https://martinfowler.com/articles/microservices.html#CharacteristicsOfAMicroserviceArchitecture">Définition d'après Martin Fowler</a> </p>
<h2>Version de langages programmations </h2>
<ul>
    <li>
        Java Spring boot : 2.3.4.RELEASE
    </li>
    <li>
        Angular : 10
    </li>
    <li>
        Java : 11
    </li>
    <li>
        Docker : 19.03.13
    </li>
    <li>
        Nodejs : 10.16.0
    </li>
</ul>

<h2>JAVA</h2>
Dans la première grand partie, je vais parler sur le travail en JAVA. 

<ul>
<li>Spring boot : 
Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".
<p><a href="https://spring.io/projects/spring-boot">source</a></p>
</li>
<li>
Spring est construit par Maven : Apache Maven is a software project management and comprehension tool. Based on the concept of a project object model (POM), Maven can manage a project's build, reporting and documentation from a central piece of information.
<a href="http://maven.apache.org/">source</a> 
    <ul>
        <li> Maven est un moteur de la gestion d'application</li>
        <li>Il permet de gérer les dépendences</li>
        <li>Il facilite le processus de construction</li>
        <li>Deployer des applications</li>
        <li>Automatisation les tests unitaires</li>
        <li>Compilation</li>
        <li>Maven crée automatiquement pom.xml, c'est un fichier qui permet de gérer l'ensemble de projet, ce fichier se trouve à la racine de projet.</li>
        <li> POM construit tois éléments :
            <ul>
                <li> Project : la définition du projet lui-même
                </li>
                <li>Dependencies : ce sont des bibliothèques de Spring (je vais lister les bibliothèques que j'ai installées)</li>
                <li>Build : permet compiler le projet </li>
            </ul>
        </li>
    </ul>
</li>
<li>
    Les bibliothèques que j'ai utilisées sur ce projet : 
    <ul>
        <li>spring-boot-starter-data-jpa : elle permet de communiquer entre la base de donnée et l'ojet d'une classe</li>
        <li>spring-boot-starter-web : Création application web, parmi j'ai utilisé est API Rest</li>
        <li>spring-boot-starter-security : c'est un componenet de sécurité, c'est pour par exemple protéger API Rest</li>
        <li>spring-boot-starter-hateoas : facilite le travail d'API</li>
        <li>spring-boot-starter-validation : vérifie les validations</li>
        <li>spring-boot-devtools : Outil développement</li>
        <li>H2 Hibernate : base de donnée </li>
        <li>spring-boot-starter-test : Test unitaire</li>
    </ul>
</li>
<li>
    Organisation du travail : Chaque package a sa propre fonctionnement 
    <ul>
    <li>Controller : contient deux controllers : BookController et CategoryController</li>
    <li>Model : contient des classes</li>
    <li>Repository : contient deux repositories : BookRepository et CategoryRepository</li>
    <li>Exception : contient trois exceptions personalisées</li>
    <li>Crypto : contient un fichier qui permet génrer un BCryptEncoder pour protéger le mot de passe</li>
    <li>Service : contient un service : BookService</li>
    <li>JWT : framework de json web token</li>
    </ul>
</li>
<li>
    Les routes (CRUD Book) : 
    <ul>
        <li>Lister tous les books : localhost:8080/all/books </li>
        <li>Récupérer un book : localhost:8080/books/{id} </li>
        <li>Enregistrer un book : localhost:8080/save/books</li>
        <li>Supprimer un book : localhost:8080/delete/book/{id}</li>
        <li>Modifier un book : localhost:8080/update/book/{id}</li>
    </ul>
</li>
<li>JWT=> Json Web Token c'est pour protéger les API Rest du côté back et front
<ul>

</ul> 
</li>
<li>TDD (Test driver developement) CRUD Category</li>

</ul>


<h2>Angular</h2>
<p>Définition : Angular is an application design framework and development platform for creating efficient and sophisticated single-page apps. <a href="https://angular.io/docs">source</a> </p>
<p>J'ai installé MDB for bootstrap, un excellent framework bootstrap très avancé ! <a href="https://mdbootstrap.com/">MDB Bootstrap</a> </p>
<p>Angular est avant tout, un framework TypeScript, il travaille principalement sur la partie d'écran.</p>
<p>Angular concentre principalement sur app.module.ts, c'est dans ce fichier qu'angular traite le système.</p>
<p>
An NgModule is defined by a class decorated with @NgModule(). The @NgModule() decorator is a function that takes a single metadata object, whose properties describe the module. The most important properties are as follows.
source <a href="https://angular.io/guide/architecture-modules#ngmodule-metadata">NgModule</a>
</p>
<ul>
<li>declarations: The components, directives, and pipes that belong to this NgModule.</li>

<li>exports: The subset of declarations that should be visible and usable in the component templates of other NgModules.</li>

<li>imports: Other modules whose exported classes are needed by component templates declared in this NgModule.</li>

<li>providers: Creators of services that this NgModule contributes to the global collection of services; they become accessible in all parts of the app. (You can also specify providers at the component level.)</li>

<li>bootstrap: The main application view, called the root component, which hosts all other app views. Only the root NgModule should set the bootstrap property.</li>
</ul>

<p>Les imports que j'ai ajoutés sont : </p>
<ul>
<li>
    MDBBootstrapModule.forRoot() => Pour MDB For Bootstrap
</li>
<li>
    ToastrModule.forRoot() => Pour toastr, elle permet d'afficher notification
</li>
<li>BrowserAnimationsModule => Pour les animations</li>
<li>FormsModule => Pour les formulaires</li>
<li>HttpClientModule => Protocole HTTP pour discuter entre deux serveurs</li>
</ul>
<p>Providers </p>
<ul>
<li>J'ai ajouté une fonctionnalité qui permet sécuriser si l'utilisateur n'est pas connecté, cela s'appelle HTTP_INTERCEPTORS</li>
<li>HTTP_INTERCEPTORS : A multi-provider token that represents the array of registered HttpInterceptor objects. <a href="https://angular.io/api/common/http/HTTP_INTERCEPTORS">source</a> </li>
</ul>
<p>J'ai créée quatres services : </p>
<ul>
<li>Data : récupérer les données</li>
<li>Http : Sécuriser authentification</li>
<li>Authentication : authentifier utilisateur</li>
<li>Router Guard : Sécuriser le site</li>
</ul>
<p> Les routes : </p>
<ul>
<li>Connexion : localhost:4200/ ou localhost:4200/login</li>
<li>Lister les books : localhost:4200/book </li>
<li>Modifier un book : localhost:4200/modify/book/:id</li>
<li>Ajouter un book : localhost:4200/add/book</li>
<li>Déconnexion : localhost:4200/logout</li>
<li>Afficher category : localhost:4200/category (vide, en cours construction)</li>
<li>Erreur par defaut : localhost:4200/**</li>
</ul>

<h2>Docker</h2>
J'ai créée deux dockerfiles, une pour java et autre pour angular.
<p>Docker : <a href="https://hub.docker.com/repository/docker/testdemo1900/bookfront">Book front</a></p>
<p>Pour récupérer l'image de book front : docker pull testdemo1900/bookfront:v10 </p>
<p>Pour démarrer l'image docker la partie front : docker run --rm -d  -p 4200:80/tcp testdemo1900/bookfront:v10 </p>
<p>Docker : book back <a href="https://hub.docker.com/repository/docker/testdemo1900/bookback">Book Back</a></p>
<p>Pour récupérer l'image de book back : docker pull testdemo1900/bookback:v10</p>
<p>Pour démarrer l'image docker la partie back : docker run -p 8081:8081 testdemo1900/bookback:v10 </p>

<h2>Points améliorations</h2>
<table>
    <thead>
        <tr>
            <th>Point à améliorer</th>
            <th align="center">Pas encore fait</th>
            <th align="center">En cours</th>
            <th align="right">Finalisé</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Organisation (enlever les console.log, enlever les codes dures, bien nommer les fichiers, réduire les lignes, corriger les fautes orthographes et ajouter les commentaires)</td>
            <td align="center"></td>
            <td align="right">X</td>
            <td></td>
        </tr>
        <tr>
            <td>Ajouter la relation entre category et book (manyTomany)</td>
            <td align="center"></td>
            <td align="right"></td>
            <td>X</td>
        </tr>
        <tr>
            <td>Ajouter Test Unitaire (TDD pour CRUD category)</td>
            <td align="center"></td>
            <td align="right"></td>
            <td>X</td>
        </tr>
        <tr>
            <td>Améliorer docker-compose (executer deux dockerfiles en même temps)</td>
            <td align="center"></td>
            <td align="right"></td>
            <td>X</td>
        </tr>
        <tr>
            <td>Améliorer de coté sécurité(spring security et JWT) et créer un registrer pour les nouvraux utilisateurs avec JWT</td>
            <td align="center"></td>
            <td align="right"></td>
            <td>X</td>
        </tr>
    </tbody>
</table>
<p>Estimation de finir le projet : Mercredi 18 novembre 2020</p>

<h2>Outils</h2>
<p>Éditeur du texte</p>
<ul>
    <li>Idea Intellij </li>
    <li>Eclipse</li>
    <LI>Visual Studio Code</LI>
    <li>Postman</li>
</ul>
<p>Outil développement</p>
<ul>
<li>Git</li>
</ul>
<h2>Sources</h2>
<p><a href="https://jwt.io/">JWT</a> : Comprendre l'ensemble sur Json Web Token</p>
<p><a href="https://start.spring.io/">Spring initialize </a>: le point départ de la création d'un projet de Spring</p>
<p><a href="https://spring.io/">Spring</a> : Documentation général sur Spring</p>
<p><a href=https://angular.io/">Angular</a> : Documentation général sur Angular</p>
<p><a href="https://mdbootstrap.com/">MDB</a> : Conception matérielle pour bootstrap</p>
<h2>Conclusion</h2>
<label>
<textarea>Voilà j'ai fait un mini projet, ce n'est pas complet et ce n'est pas avancé non plus, je montre mon minimum les codes, les fonctionnalités et les techniques utilisées.
Ce qui est sûre, dans les prochaines jours, prochains mois, prochainnes années, je vais sans doute améliorer mes qualités de codes et les expériences sur le développement full stack, en fin, ce qui compte pour réussir le métier, je résume sur ces éléments que j'ai appris: 
</textarea>
</label>
<ul>
    <li>Bien comprendre l'objectif</li>
    <li>Analyse et l'étude sur le projet</li>
    <li>Améliorer les documentations et améliorer la qualité de codes</li>
    <li>Réduire l'écriture du code</li>
    <li>Apprentissage</li>
 
</ul>