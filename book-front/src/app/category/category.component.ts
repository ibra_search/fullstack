import { Component, OnInit } from '@angular/core';
import { CategoryDataService } from '../service/data/category-data.service';
import { Router } from '@angular/router';
import {Category} from '../model/category';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  category: Category 
  constructor(
    private router : Router,
    private categoryService: CategoryDataService
  ) { }

  ngOnInit(): void {
    this.categoryService.getAllCategories().subscribe(
      response => {
        
      }
    )
  }

  refreshCategories(){
      
  }

  submitCategory(){
    this.categoryService.saveCategory(this.category).subscribe(
      data => {
        
      }
    )
  }

}
