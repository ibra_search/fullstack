import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../model/book';
import { Category } from '../model/category';
import { BookDataService } from '../service/data/book-data.service';
import { CategoryDataService } from '../service/data/category-data.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss']
})
export class AddBookComponent implements OnInit {


  id:number;
  book = {
    name: '',
    description:'',
    categories : {
      type : ''
    }
    
  }


  submitted = false;
  constructor(
    private bookService : BookDataService,
    private categoryService : CategoryDataService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

  }

  
 
  submitFunc(){
    const data = {
      name: this.book.name,
      description: this.book.description,
      categories : [
        this.book.categories.type
      ]
    }

    this.bookService.createBook(data).subscribe(
      response => {
        this.submitted = true;
        this.router.navigate(['book']);
      }, 
      error=>{
        console.log(error);
      }      
    )
  }
}
