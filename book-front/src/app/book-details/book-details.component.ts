import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookDataService } from '../service/data/book-data.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.scss']
})
export class BookDetailsComponent implements OnInit {

  currentBook = null;
  message= '';

  book = {
    id: null,
    name: '',
    description:'',
    categories : {
      type : ''
    }
    
  }

  constructor(
    private service: BookDataService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.message='';
    this.getBookId(this.route.snapshot.paramMap.get('id'));

  }

  getBookId(id): void{
    this.service.getBookId(id).subscribe(
      data => {
        this.book = data;
      },
      error => {
        console.log(error);
      }
    )

  }

  updateBook(): void{

    this.service.updateBook(this.book.id, this.book).subscribe(
        response => {
          this.toastr.success("Updated succesfully !")
          setTimeout(()=> {
            this.router.navigate(['book']);
          }, 5000);
          
        },
        error => {
          this.toastr.error("Erreur : Vérifier vos données");
          
        }
    )
  }
}
