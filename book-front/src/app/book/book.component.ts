import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BookDataService } from '../service/data/book-data.service';
import { Book } from '../model/book';
import { ThrowStmt } from '@angular/compiler';
import { MdbTableDirective, MdbTablePaginationComponent } from 'angular-bootstrap-md';
import { Category } from '../model/category';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit{
  @ViewChild('alert', { static: true }) alert: ElementRef;
  headElements = ['ID', 'Name', 'Description','Category', 'Actions'];


  message: string
  books : Book[];
  Category : Category[];

  book = {
    name: '',
    description:'',
    category: Category 
  }
  constructor(
    private router : Router,
    private bookService : BookDataService,
    private cdRef: ChangeDetectorRef,
    private toastr: ToastrService
  ) { }

  ngOnInit(){
    
    this.bookService.getAllBooks().subscribe(
      data => {
        this.books = data
      }
    );
  }

  refreshBooks(){
    this.bookService.getAllBooks().subscribe(
      response => {
        
        this.books = response;
        
      }
    )
  }

  deleteBook(id){
    this.bookService.deleteBook(id).subscribe(
      response => {
        this.toastr.success("Delete of Book " + id + " successful!")
        setTimeout(()=> {
        }, 1000);
        this.refreshBooks();
      } 
    )
  }


  closeAlert() {
    this.alert.nativeElement.classList.remove('show');
  }
}
