import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { BookComponent } from './book/book.component';
import { CategoryComponent } from './category/category.component';
import { AddBookComponent } from './add-book/add-book.component';
import { LoginComponent } from './authentication/login/login.component';
import { ErrorComponent } from './exception/error/error.component';
import { LogoutComponent } from './authentication/logout/logout.component';
import {RouteguardService} from './service/routeguard.service';
import { BookDetailsComponent } from './book-details/book-details.component';
import { RegisterComponent } from './authentication/register/register.component';

const routes: Routes = [
  {path : '', component: LoginComponent },
  {path : 'book', component: BookComponent, canActivate:[RouteguardService]},
  {path : 'add/book', component: AddBookComponent, canActivate :[RouteguardService]},
  {path : 'modify/book/:id', component : BookDetailsComponent, canActivate :[RouteguardService]},
  {path : 'login', component: LoginComponent},
  {path : 'signup', component : RegisterComponent},
  {path : 'logout', component: LogoutComponent, canActivate : [RouteguardService]},
  {path : 'category', component: CategoryComponent, canActivate : [RouteguardService]},
  {path : '**', component: ErrorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
