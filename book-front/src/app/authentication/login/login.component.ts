import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../service/authentication.service';
import { BasicauthenticationService } from '../../service/basicauthentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username = 'ibra'
  password = ''
  errorMessage = 'Invalid credentials'
  invalidLogin = false
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private basicAuthenticationService: BasicauthenticationService
  ) { }

  ngOnInit(): void {
  }

  handleLogin() {
    if(this.authenticationService.authenticate(this.username, this.password)){
      this.router.navigate(['book']);
      this.invalidLogin = false; 
    }else{
      this.invalidLogin = true;
    }
  }

  checkLogin(){
    this.authenticationService.authenticate(this.username, this.password).subscribe(
      data => {
        this.router.navigate(['book']);
        this.invalidLogin = false    

      },
      error => {
        this.invalidLogin = true

      }
    )
  }


  handleJWTAuthLogin() {
    this.authenticationService.authenticateWithJWT(this.username, this.password).subscribe(
      data => {
        this.router.navigate(['book']);
        this.invalidLogin = false    

      },
      error => {
        this.invalidLogin = true
      }
    )
  }



}
