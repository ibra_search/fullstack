import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {UserService} from '../../service/data/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user = {
    username: '',
    password:''
  }
  submitted = false;
  constructor(
    private userService : UserService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  registerFunc(){
    const data = {
      username : this.user.username,
      password : this.user.password
    }

    this.userService.createUser(data).subscribe(
      response => {
        this.submitted = true;
        this.router.navigate(['login']);
      },
      error=>{
        console.log(error);
      }      
    )
  }
}
