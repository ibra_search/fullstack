import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from '../app.constant';
import { map } from 'rxjs/operators';

export class User{
  constructor(
    public status:string,
     ) {}
  
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private httpClient:HttpClient
  ) { }


  authenticate(username, password){
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.httpClient.get<User>(`${API_URL}/auth`, {headers}).pipe(
      map(
        userData => {
         sessionStorage.setItem('username',username);
         return userData;
        }
      )
    );
  }


  authenticateWithJWT(username, password){
    return this.httpClient.post<any>(
      `${API_URL}/authenticate`,{
        username,
        password
      }).pipe(
        map(
          data => {
            sessionStorage.setItem('username', username);
            sessionStorage.setItem('token', `Bearer ${data.token}`);
            return data;
          }
        )
      );
  }

  isUserLoggedIn(){
    let user = sessionStorage.getItem('username')
    return !(user === null)
  }

  logout(){
    sessionStorage.removeItem('username')
  }
}
