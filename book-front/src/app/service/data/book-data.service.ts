import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { API_URL } from '../../app.constant';
import { Book } from '../../model/book';
import { Observable } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class BookDataService {

  

  constructor(
    private http:HttpClient
  ) { }


  getAllBooks(): Observable<Book[]> {

    return this.http.get<Book[]>(`${API_URL}/all/books`);
  }

  getBookId(id): Observable<any>{

    return this.http.get(`${API_URL}/books/${id}`);
  }

  createBook(book){

    return this.http.post<Book>(`${API_URL}/save/books`, book);
  }

  deleteBook(id): Observable<any>{

    return this.http.delete(`${API_URL}/delete/book/${id}`);
  }

  updateBook(id, book) : Observable<any>{

    return this.http.put(`${API_URL}/update/book/${id}`, book)
  }
}
