import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { API_URL } from '../../app.constant';
import { Category } from 'src/app/model/category';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class CategoryDataService {

  constructor(
    private http:HttpClient
  ) { }


  getAllCategories(){
    return this.http.get<Category[]>(`${API_URL}/all/categories`);
  }

  saveCategory(category): Observable<any>{
    return this.http.post(`${API_URL}/categories`, category);
  }
}
