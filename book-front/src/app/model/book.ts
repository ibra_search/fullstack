import { Category } from './category';

export class Book {
    constructor(
        public id: number,
        public name: string,
        public description: string,
        public categories: Category[]
      ){
    
      }
}
