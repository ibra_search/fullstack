package com.demo.book.demobookback;


import com.demo.book.demobookback.model.Category;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoBookBackApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoBookBackApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetAllCategories()
	{
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/categories",
				HttpMethod.GET, entity, String.class);

		Assert.assertNotNull(response.getBody());
	}

	@Test
	public void testGetCategoryById(){
		Category category = restTemplate.getForObject(getRootUrl() + "/categories/1", Category.class);
		System.out.println(category.getType());
		Assert.assertNotNull(category);

	}


	@Test
	public void testCreateCategory(){
		Category category = new Category();
		category.setType("romance");
		ResponseEntity<Category> postResponse = restTemplate.postForEntity(getRootUrl() + "/categories", category, Category.class);
		Assert.assertNotNull(postResponse);
		Assert.assertNotNull(postResponse.getBody());
		

	}

	@Test
	public void testUpdateCategory(){
		int id = 1;
		Category category = restTemplate.getForObject(getRootUrl()+"/categories"+id,Category.class);
		category.setType("action");

		restTemplate.put(getRootUrl() + "/categories/" + id, category);
		Category updatedCategory = restTemplate.getForObject(getRootUrl() + "/categories/" + id, Category.class);
		Assert.assertNotNull(updatedCategory);
	}

	@Test
	public void testDeleteCategory(){
		int id = 2;
		Category category = restTemplate.getForObject(getRootUrl() + "/categories/" + id, Category.class);
		Assert.assertNotNull(category);

		restTemplate.delete(getRootUrl() + "/categories/" + id);


		try {
			category = restTemplate.getForObject(getRootUrl() + "/categories/" + id, Category.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}

}
