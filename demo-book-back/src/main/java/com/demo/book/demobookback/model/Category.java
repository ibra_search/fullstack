package com.demo.book.demobookback.model;

import javax.persistence.*;


@Entity
@Table(name = "categories")
public class Category {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name="type_name", nullable=false)
    private String type;

    public Category(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Category(String type) {
        this.type = type;
    }

}
