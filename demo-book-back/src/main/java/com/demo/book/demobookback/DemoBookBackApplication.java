package com.demo.book.demobookback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoBookBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoBookBackApplication.class, args);
	}

}
