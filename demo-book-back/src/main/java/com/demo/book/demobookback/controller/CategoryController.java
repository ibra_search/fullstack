package com.demo.book.demobookback.controller;


import com.demo.book.demobookback.exception.CategoryNotFound;
import com.demo.book.demobookback.model.Category;
import com.demo.book.demobookback.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class CategoryController {


    @Autowired
    CategoryRepository repository;


    @GetMapping("/categories")
    public List<Category> getAllCategories(){
        return repository.findAll();
    }


    @GetMapping("/categories/{id}")
    public ResponseEntity<Category> getCategoriesById(@PathVariable(value = "id") Long categoryId) throws CategoryNotFound {

            Category category = repository.findById(categoryId).orElseThrow(()
            -> new CategoryNotFound("Category not found on :" + categoryId)
            );
        return ResponseEntity.ok().body(category);

    }


    @PostMapping("/categories")
    public ResponseEntity<Category> createCategories(@RequestBody Category category){
        Category savedCategory = repository.save(category);

        return  new ResponseEntity<Category>(category,HttpStatus.OK);
    }


    @PutMapping("/categories/{id}")
    public ResponseEntity<Category> updateCategory(@PathVariable(value = "id") Long categoryId, @Valid @RequestBody Category categoryDetails)  throws CategoryNotFound {

        Category category = repository.findById(categoryId).orElseThrow(()
                -> new CategoryNotFound("Category not found on :" + categoryId)
        );

        category.setType(categoryDetails.getType());
        final Category updatedCategory = repository.save(category);
        return ResponseEntity.ok(updatedCategory);
    }

    @DeleteMapping("/categories/{id}")
    public ResponseEntity<HttpStatus> deleteCategory(@PathVariable("id") long id) throws CategoryNotFound {
        Category category = repository.findById(id).orElseThrow(()
                -> new CategoryNotFound("Category not found on :" + id)
        );

        repository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
