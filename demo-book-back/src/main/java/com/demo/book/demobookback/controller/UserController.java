package com.demo.book.demobookback.controller;

import com.demo.book.demobookback.model.User;
import com.demo.book.demobookback.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ValidationException;
import java.util.Map;

@CrossOrigin(origins="http://localhost:4200")
@RestController
public class UserController {

    @Value("${user.get.role}")
    String role;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @PostMapping("/signup")
    public Boolean create(@RequestBody Map<String, String> body) {
        String username = body.get("username");
        if (userRepository.existsByUsername(username)){

            throw new ValidationException("Username already existed");

        }
        String password = body.get("password");
        String encodedPassword = new BCryptPasswordEncoder().encode(password);


        userRepository.save(new User(username, encodedPassword, role));
        return true;
    }
}
