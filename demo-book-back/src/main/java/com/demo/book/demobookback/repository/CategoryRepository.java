package com.demo.book.demobookback.repository;

import com.demo.book.demobookback.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}
