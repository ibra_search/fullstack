package com.demo.book.demobookback.service;

import org.springframework.stereotype.Component;

import com.demo.book.demobookback.model.Book;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class BookService {
	 private static List<Book> books = new ArrayList<>();
	    
	    // That method get all books
	    public List<Book> findAllBooks(){
	        return books;
	    }

	    // that method save book
	    public Book saveBook(Book book){
	        books.add(book);
	        return  book ;
	    }

	    //that methode get one book
	    public Book findBook(int id){
	        for(Book book : books){
	            if(book.getId()==id){
	                return book;
	            }
	        }
	        return null;
	    }

	    //that methode delete one book
	    public Book deleteBook(int id){
	        Iterator<Book> iterator = books.iterator();
	        while(iterator.hasNext()){
	            Book book = iterator.next();
	            if(book.getId()==id){
	                iterator.remove();
	                return book;
	            }
	        }
	        return null;
	    }
	
}
