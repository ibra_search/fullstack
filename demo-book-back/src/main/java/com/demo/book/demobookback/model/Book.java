package com.demo.book.demobookback.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "books")
public class Book {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Size(min=3, message = "Name should had at least 3 characters")
	private String name;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable
	private Set<Category> categories = new HashSet<>();

	private String description;

	public Book(){
	}

	public Book(String name, String description) {
		this.name = name;
		this.description = description;
	}
	public long getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}
}
