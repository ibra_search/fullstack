package com.demo.book.demobookback.controller;

import com.demo.book.demobookback.exception.BookNotFoundException;
import com.demo.book.demobookback.model.Book;
import com.demo.book.demobookback.repository.BookRepository;
import com.demo.book.demobookback.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@CrossOrigin(origins="http://localhost:4200")
@RestController
public class BookController {
	
	
	@Autowired
	private BookRepository repository;

	@Autowired
    private BookService service;
	
	@GetMapping("/all/books")
	public List<Book> getAllBooks(){



	    return repository.findAll();
	}

	@GetMapping("/books/{id}")
	public EntityModel<Book> getBookId(@PathVariable("id") long id){
		Optional<Book> getBooksId = repository.findById(id);

		if(getBooksId.isPresent()){
			//"all-books"
			//getAllBooks()
			EntityModel<Book> resource = EntityModel.of(getBooksId.get());

			WebMvcLinkBuilder linkTo =
					linkTo(methodOn(this.getClass()).getAllBooks());

			resource.add(linkTo.withRel("all-books"));

			//HATEOAS
			return resource;
		}else{
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			throw new BookNotFoundException("id - " + id);
		}

	}
	
    @PostMapping("/save/books")
    public ResponseEntity<Book> saveBook(@Valid @RequestBody Book book){
	    try{
        //add resource
        	Book savedBook = repository.save(book);

        //Create Location and Get current resource url
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(savedBook.getId()).toUri();
        return ResponseEntity.created(uri).build();
	    }catch (Exception e){
	        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/delete/book/{id}")
	public ResponseEntity<HttpStatus> deleteBook(@PathVariable("id") long id){
		try{
			repository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}catch(Exception e){
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@PutMapping("/update/book/{id}")
	public ResponseEntity<Book> updateBook(@PathVariable("id") long id, @RequestBody Book book) {
		Optional<Book> bookModify = repository.findById(id);
		if(bookModify.isPresent()){
			Book _book = bookModify.get();
			_book.setName(book.getName());
			_book.setDescription(book.getDescription());
			_book.setCategories(book.getCategories());
			return new ResponseEntity<>(repository.save(_book), HttpStatus.OK);
		}else{
			return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
